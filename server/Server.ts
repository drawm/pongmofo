import {EventEmitter} from 'events';
import Rectangle from './utils/Rectangle';
import BallLogic from './BallLogic';
import ScoreBoard from "./scoreBoard/ScoreBoard";

export default class Server {
  private ballLogic: BallLogic;
  private scoreBoard: ScoreBoard;

  constructor(
    private gameEvent: EventEmitter,
    stage: Rectangle,
    playerA: Rectangle = new Rectangle(),
    playerB: Rectangle = new Rectangle(),
    ball: Rectangle = new Rectangle(),
  ) {
    this.ballLogic = new BallLogic(this.gameEvent, ball, playerA, playerB, stage, BallLogic.getRandomVelocity());
    this.scoreBoard = new ScoreBoard(this.gameEvent);
  }

  tick() {
    this.ballLogic.move();
  }

}