import {EventEmitter} from 'events';
import Rectangle from "../utils/Rectangle";
import PlayerController from "./controllers/PlayerController";

export default class DumbPlayerAI extends PlayerController {
  private distanceThreshold: number = 12;

  constructor(controller: EventEmitter, private ball: Rectangle, player: Rectangle, gameHeight:number) {
    super(controller, player, gameHeight);
  }

  public move(): void {
    if(Math.abs(this.player.x - this.ball.x) > 350) {
      return;
    }

    const playerCenter : number = this.player.y + (this.player.height >> 1);
    const distanceToBall : number = this.ball.y - playerCenter;

    if (distanceToBall < -this.distanceThreshold) {
      this.moveDown();

    } else if(distanceToBall > this.distanceThreshold) {
      this.moveUp();
    }
  }
}