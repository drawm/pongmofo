import {expect} from 'chai';
import {EventEmitter} from 'events';
import BallLogic from '../BallLogic';
import Rectangle from "../utils/Rectangle";
import Vector from "../utils/Vector";
import BallEvents from "../BallEvents";
import * as Sinon from 'sinon';
import GameEvents from "../GameEvents";

const isBall = (x, y, position) => ( x === Math.floor(position.x) && y === Math.floor(position.y) );
const isPaddle = (x, y, paddle) => {
  return x >= paddle.x && x <= paddle.x + paddle.width - 1 &&
    y >= paddle.y && y <= paddle.y + paddle.height - 1;
};
const printGrid = (position, maxWidth, maxHeight, paddleA = null, paddleB = null) => {
  let output = '';
  let x = 0;
  let y = 0;

  do {
    output += '|';
    do {
      if (isBall(x, y, position)) {
        output += ' 0 ';
      } else if (paddleA && isPaddle(x, y, paddleA)) {
        output += ' = ';
      } else if (paddleB && isPaddle(x, y, paddleB)) {
        output += ' 8 ';
      } else {
        output += ' . ';
      }
    } while (x++ < maxWidth);

    x = 0;
    output += '|';
    output += '\n';
  } while (y++ < maxHeight);

  console.log(output);
};

describe('BallLogic', () => {
  let ballLogic: BallLogic;

  let eventEmitter: EventEmitter;
  let ball: Rectangle;
  let playerA: Rectangle;
  let playerB: Rectangle;
  let stage: Rectangle;
  let velocity: Vector;

  beforeEach(() => {
    eventEmitter = new EventEmitter();
    ball = new Rectangle(2, 2, 1, 1);
    playerA = new Rectangle(-100, 0, 0, 0);
    playerB = new Rectangle(-100, 0, 0, 0);
    stage = new Rectangle(0, 0, 5, 5);
  });

  describe('collision with paddles', () => {
    beforeEach(() => {
      ball = new Rectangle(2, 1, 1, 1);
      velocity = new Vector(1, 0);
      stage = new Rectangle(0, 0, 7, 3);
      playerA = new Rectangle(1, 0, 1, 3);
      playerB = new Rectangle(5, 0, 1, 3);

      ballLogic = new BallLogic(
        eventEmitter,
        ball,
        playerA,
        playerB,
        stage,
        velocity,
        3,
      );
    });

    it('bounce of each paddles', () => {
      const ballMoveSpy = Sinon.spy();
      const playerAScore = Sinon.spy();
      const playerBScore = Sinon.spy();
      eventEmitter.on(BallEvents.move, ballMoveSpy);
      eventEmitter.on(GameEvents.playerAScore, playerAScore);
      eventEmitter.on(GameEvents.playerBScore, playerBScore);
      eventEmitter.on(BallEvents.move, position => printGrid(position, stage.width - 1, stage.height - 1, playerA, playerB));

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(3, 1));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(4, 1));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(5, 1));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 1));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(5, 1));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 1));
      ballMoveSpy.reset();

      Sinon.assert.notCalled(playerAScore);
      Sinon.assert.notCalled(playerBScore);
    });
  });

  describe('movement along the y axis', () => {
    beforeEach(() => {
      velocity = new Vector(0, 1);

      ballLogic = new BallLogic(
        eventEmitter,
        ball,
        playerA,
        playerB,
        stage,
        velocity,
        1,
      );
    });

    it('initialise properly', () => {
      expect(ballLogic).to.not.be.undefined;
    });

    it('bounce off walls', () => {
      const ballMoveSpy = Sinon.spy();
      const playerAScore = Sinon.spy();
      const playerBScore = Sinon.spy();
      eventEmitter.on(BallEvents.move, ballMoveSpy);
      eventEmitter.on(GameEvents.playerAScore, playerAScore);
      eventEmitter.on(GameEvents.playerBScore, playerBScore);
      // eventEmitter.on(BallEvents.move, position => printGrid(position, stage.width - 1, stage.height - 1));

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 3));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.notCalled(playerAScore);
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 4));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 3));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 2));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 1));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 0));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 1));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 2));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 3));
      ballMoveSpy.reset();

      Sinon.assert.notCalled(playerAScore);
      Sinon.assert.notCalled(playerBScore);
    });

    it('move the ball according to its velocity', () => {
      const ballMoveSpy = Sinon.spy();
      eventEmitter.on(BallEvents.move, ballMoveSpy);

      ballLogic.move();
      Sinon.assert.calledOnce(ballMoveSpy);
      let expectedPosition = Vector.plus(ball as Vector, velocity);
      Sinon.assert.calledWithExactly(ballMoveSpy, expectedPosition);
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, Vector.plus(expectedPosition, velocity));
    });

  });

  describe('movement along the x axis', () => {
    beforeEach(() => {
      velocity = new Vector(1, 0);

      ballLogic = new BallLogic(
        eventEmitter,
        ball,
        playerA,
        playerB,
        stage,
        velocity,
        1,
      );
    });

    it('initialise properly', () => {
      expect(ballLogic).to.not.be.undefined;
    });

    it('bounce off walls', () => {
      const ballMoveSpy = Sinon.spy();
      const playerAScore = Sinon.spy();
      const playerBScore = Sinon.spy();
      eventEmitter.on(BallEvents.move, ballMoveSpy);
      eventEmitter.on(GameEvents.playerAScore, playerAScore);
      eventEmitter.on(GameEvents.playerBScore, playerBScore);
      // eventEmitter.on(BallEvents.move, position => printGrid(position, stage.width - 1, stage.width-1));

      Sinon.assert.notCalled(playerAScore);
      Sinon.assert.notCalled(playerBScore);


      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(3, 2));
      ballLogic.move();
      Sinon.assert.notCalled(playerAScore);
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(4, 2));
      ballLogic.move();
      Sinon.assert.called(playerAScore);
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(3, 2));
      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 2));
      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(1, 2));
      ballLogic.move();
      Sinon.assert.notCalled(playerBScore);
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(0, 2));
      ballLogic.move();
      Sinon.assert.called(playerBScore);
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(1, 2));
      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 2));
      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(3, 2));

      Sinon.assert.calledOnce(playerAScore);
      Sinon.assert.calledOnce(playerBScore);
    });

    it('move the ball according to its velocity', () => {
      const ballMoveSpy = Sinon.spy();
      eventEmitter.on(BallEvents.move, ballMoveSpy);

      ballLogic.move();
      Sinon.assert.calledOnce(ballMoveSpy);
      let expectedPosition = Vector.plus(ball as Vector, velocity);
      Sinon.assert.calledWithExactly(ballMoveSpy, expectedPosition);

      ballLogic.move();
      Sinon.assert.calledTwice(ballMoveSpy);
      Sinon.assert.calledWithExactly(ballMoveSpy, Vector.plus(expectedPosition, velocity));
    });
  });

  describe('movement both axis', () => {
    beforeEach(() => {
      velocity = new Vector(1, 2);
      ball = new Rectangle(2, 2, 1, 1);

      ballLogic = new BallLogic(
        eventEmitter,
        ball,
        playerA,
        playerB,
        stage,
        velocity,
        1,
      );
    });

    it('initialise properly', () => {
      expect(ballLogic).to.not.be.undefined;
    });

    it('bounce off walls', () => {
      const ballMoveSpy = Sinon.spy();
      const playerAScore = Sinon.spy();
      const playerBScore = Sinon.spy();
      eventEmitter.on(BallEvents.move, ballMoveSpy);
      eventEmitter.on(GameEvents.playerAScore, playerAScore);
      eventEmitter.on(GameEvents.playerBScore, playerBScore);

      // console.log()
      // printGrid(ball, stage.width - 1, stage.width-1);
      // eventEmitter.on(BallEvents.move, position => printGrid(position, stage.width - 1, stage.width-1));

      Sinon.assert.notCalled(playerAScore);
      Sinon.assert.notCalled(playerBScore);

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(3, 4));
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.notCalled(playerAScore);
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(4, 2));
      ballMoveSpy.reset();
      ballLogic.move();
      Sinon.assert.called(playerAScore);
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(3, 0));
      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 2));
      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(1, 4));
      ballLogic.move();
      Sinon.assert.notCalled(playerBScore);
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(0, 2));
      ballLogic.move();
      Sinon.assert.called(playerBScore);
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(1, 0));
      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(2, 2));
      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, new Vector(3, 4));

      Sinon.assert.calledOnce(playerAScore);
      Sinon.assert.calledOnce(playerBScore);
    });

    it('move the ball according to its velocity', () => {
      const ballMoveSpy = Sinon.spy();
      eventEmitter.on(BallEvents.move, ballMoveSpy);

      ballLogic.move();
      Sinon.assert.calledOnce(ballMoveSpy);
      let expectedPosition = Vector.plus(ball as Vector, velocity);
      Sinon.assert.calledWithExactly(ballMoveSpy, expectedPosition);
      ballMoveSpy.reset();

      ballLogic.move();
      Sinon.assert.calledWithExactly(ballMoveSpy, Vector.plus(expectedPosition, velocity));
    });
  });
});