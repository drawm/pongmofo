const GameEvents =  {
  playerBScore:'playerBScore',
  playerAScore:'onPlayerAScore',
  teamAWin:'onPlayerAWin',
  teamBWin:'onPlayerBWin',
  reset:'reset',
};

export default GameEvents;

