import {EventEmitter} from 'events';
import Server from './Server';
import Rectangle from "./utils/Rectangle";
import NetworkEmitter from "./NetworkEmitter";

const eventEmitter = new NetworkEmitter({
      port : 4000,
      encoder : 'json',
});

const ball = new Rectangle(17, 17, 20, 20);
const playerA = new Rectangle(1, 0, 1, 35);
const playerB = new Rectangle(32, 0, 1, 35);
const stage = new Rectangle(0, 0, 800, 500);

const server = new Server(eventEmitter, stage, playerA, playerB, ball);

setInterval(() => {
    server.tick();
}, 24);
