import * as Kalm from 'kalm';
import * as ws from 'kalm-websocket';
import {EventEmitter} from 'events';

export default class NetworkEmitter extends EventEmitter {

    private kalm;

    constructor(options) {
        super();
        Kalm.adapters.register('ws', ws);

        this.kalm = new Kalm.Server({
            ...options,
            adapter : 'ws'
        });
    }

    on(event: string | symbol, listener: Function): this {
        super.on(event, listener);
        this.kalm.subscribe(event, listener);
        return this;
    }

    emit(event: string | symbol, ...args: any[]): boolean {
        const success: boolean = super.emit(event, args);
        this.kalm.broadcast(event, args);
        return success;
    }
}
