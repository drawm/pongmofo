import {EventEmitter} from 'events';
import PlayerController from "./PlayerController";
import Rectangle from "../../utils/Rectangle";

enum Direction {
  up,
  down,
  idle,
}
export default class KeyboardController extends PlayerController {
  private direction: Direction;

  constructor(controller: EventEmitter, player: Rectangle, gameHeight: number) {
    super(controller, player, gameHeight);

    window.addEventListener('keydown', this.keyDown.bind(this));
    window.addEventListener('keyup', this.keyUp.bind(this));
  }

  private keyDown(event: KeyboardEvent): void {
    if (event.keyCode === 87) {
      this.direction = Direction.down;
    } else if (event.keyCode === 83) {
      this.direction = Direction.up;
    }
  }

  private keyUp(event: KeyboardEvent): void {
    if (event.keyCode === 87 && this.direction === Direction.down) {
      this.direction = Direction.idle;
    } else if (event.keyCode === 83 && this.direction === Direction.up) {
      this.direction = Direction.idle;
    }
  }

  move() {
    if (this.direction === Direction.down) {
      this.moveDown();
    } else if (this.direction === Direction.up) {
      this.moveUp();
    }

  }
}