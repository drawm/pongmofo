import {EventEmitter} from 'events';
import Rectangle from "../../utils/Rectangle";
import PlayerEvents from "../../player/PlayerEvents";
import Vector from "../../utils/Vector";

export default class PlayerController {
  private speed:number = 5;

  constructor(private controller: EventEmitter, protected player:Rectangle, private gameHeight:number) {

  }

  private dispatchPosition(){
    this.controller.emit(PlayerEvents.move, new Vector(this.player.x, this.player.y));
  }

  protected moveUp(){
    const newY = this.player.y + this.speed;
    if(newY < this.gameHeight - this.player.height){
      this.player.y = newY;
      this.dispatchPosition();
    }
  }

  protected moveDown(){
    const newY = this.player.y - this.speed;
    if(newY > 0){
      this.player.y = newY;
      this.dispatchPosition();
    }
  }
}