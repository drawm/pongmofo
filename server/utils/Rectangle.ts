import Vector from "./Vector";
export default class Rectangle {
  constructor(public x: number = 0, public y: number = 0, public width: number = 0, public height: number = 0) {
  }

  public static dirtyCollision(A: Rectangle, B: Rectangle): boolean {
    // Find out if rectangle A and B touches
    const aCenterX: number = A.x + (  A.width >> 1);
    const bCenterX: number = B.x + (  B.width >> 1);

    const aCenterY: number = A.y + ( A.height >> 1);
    const bCenterY: number = B.y + ( B.height >> 1);

    return (Math.abs(aCenterX - bCenterX) <= Math.max(A.width, B.width) >> 1 &&
      ( Math.abs(aCenterY - bCenterY) <= Math.max(A.height, B.height) >> 1))

  }

  public static willTraverse(A: Rectangle, velocity:Vector, B: Rectangle): boolean {

    // Find out if rectangle A and B touches
    const startCenter = new Vector(A.x + (  A.width >> 1), A.y + ( A.height >> 1));
    const bCenter = new Vector(B.x + ( B.width >> 1),B.y + ( B.height >> 1) );

    // Find out if rectangle A will be on the other side of rectangle B
    const movedCenter = new Vector(startCenter.x + velocity.x, startCenter.y + velocity.y);

    // If the distance is positive or negative before and after the move
    const traversedX: boolean = startCenter.x - bCenter.x > 0 !== movedCenter.x - bCenter.x > 0;
    const traversedY: boolean = startCenter.y - bCenter.y > 0 !== movedCenter.y - bCenter.y > 0;

    return traversedX && traversedY;
  }
}