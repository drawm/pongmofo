export default class Vector {
  static zero: Vector = new Vector(0, 0);
  static left: Vector = new Vector(-1, 0);
  static right: Vector = new Vector(1, 0);
  static up: Vector = new Vector(0, -1);
  static down: Vector = new Vector(0, 1);

  constructor(public x: number, public y: number) {
  }

  static times(multiplier: number, vector: Vector) {
    return new Vector(multiplier * vector.x, multiplier * vector.y);
  }

  static minus(vectorA: Vector, vectorB: Vector) {
    return new Vector(vectorA.x - vectorB.x, vectorA.y - vectorB.y);
  }

  static plus(vectorA: Vector, vectorB: Vector) {
    return new Vector(vectorA.x + vectorB.x, vectorA.y + vectorB.y);
  }

  static dot(vectorA: Vector, vectorB: Vector) {
    return vectorA.x * vectorB.x + vectorA.y * vectorB.y;
  }

  static mag(vector: Vector) {
    return Math.sqrt(vector.x * vector.x + vector.y * vector.y);
  }

  static norm(vector: Vector) {
    const mag = Vector.mag(vector);
    const div = (mag === 0) ? Infinity : 1.0 / mag;
    return Vector.times(div, vector);
  }

  static cross(vectorA: Vector, vectorB: Vector) {
    return new Vector(
      vectorA.y * vectorB.x - vectorA.x * vectorB.y,
      vectorA.x * vectorB.y - vectorA.y * vectorB.x
    );
  }

  static collide(a:Vector,b:Vector,c:Vector,d:Vector):boolean{
    const aSide = (d.x - c.x) * (a.y - c.y) - (d.y - c.y) * (a.x - c.x) > 0;
    const bSide = (d.x - c.x) * (b.y - c.y) - (d.y - c.y) * (b.x - c.x) > 0;
    const cSide = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x) > 0;
    const dSide = (b.x - a.x) * (d.y - a.y) - (b.y - a.y) * (d.x - a.x) > 0;
    return aSide !== bSide && cSide !== dSide;
  }

  toString(){
    return `{ x:${this.x}, y:${this.y} }`;
  }

}