import {expect} from 'chai';
import {EventEmitter} from 'events';
import ScoreBoard from '../ScoreBoard';
import ScoreBoardsEvents from '../ScoreBoardEvents';
import * as Sinon from 'sinon';
import GameEvents from "../../GameEvents";

describe('ScoreBoard', () => {
  let scoreBoard:ScoreBoard;
  let eventEmitter:EventEmitter;

  beforeEach(() => {
    eventEmitter = new EventEmitter();
    scoreBoard = new ScoreBoard(eventEmitter);
  });

  it('initialise properly', () => {
    expect(scoreBoard).to.not.be.undefined;
  });

  it('dispatch an event when someone scores', () => {
    const teamAScoreSpy = Sinon.spy();
    eventEmitter.on(ScoreBoardsEvents.updateTeamAScore, teamAScoreSpy);

    const teamBScoreSpy = Sinon.spy();
    eventEmitter.on(ScoreBoardsEvents.updateTeamBScore, teamBScoreSpy);

    Sinon.assert.notCalled(teamBScoreSpy);

    eventEmitter.emit(GameEvents.playerAScore);
    Sinon.assert.calledOnce(teamAScoreSpy);


    eventEmitter.emit(GameEvents.playerBScore);
    Sinon.assert.calledOnce(teamBScoreSpy);
  });

  it('dispatch an event when team A wins the game', () => {
    const teamAScoreSpy = Sinon.spy();
    eventEmitter.on(ScoreBoardsEvents.updateTeamAScore, teamAScoreSpy);
    const teamAWinSpy = Sinon.spy();
    eventEmitter.on(GameEvents.teamAWin, teamAWinSpy);

    const teamBScoreSpy = Sinon.spy();
    eventEmitter.on(ScoreBoardsEvents.updateTeamBScore, teamBScoreSpy);

    let i = ScoreBoard.POINTS_REQUIRED_TO_WIN;
    while(i--){
      eventEmitter.emit(GameEvents.playerAScore);
    }
    Sinon.assert.callCount(teamAScoreSpy, ScoreBoard.POINTS_REQUIRED_TO_WIN);
    Sinon.assert.calledOnce(teamAWinSpy);


    Sinon.assert.notCalled(teamBScoreSpy);
  });

  it('dispatch an event when team B wins the game', () => {
    const teamBScoreSpy = Sinon.spy();
    eventEmitter.on(ScoreBoardsEvents.updateTeamBScore, teamBScoreSpy);
    const teamBWinSpy = Sinon.spy();
    eventEmitter.on(GameEvents.teamBWin, teamBWinSpy);

    const teamAScoreSpy = Sinon.spy();
    eventEmitter.on(ScoreBoardsEvents.updateTeamAScore, teamAScoreSpy);

    let i = ScoreBoard.POINTS_REQUIRED_TO_WIN;
    while(i--){
      eventEmitter.emit(GameEvents.playerBScore);
    }
    Sinon.assert.callCount(teamBScoreSpy, ScoreBoard.POINTS_REQUIRED_TO_WIN);
    Sinon.assert.calledOnce(teamBWinSpy);


    Sinon.assert.notCalled(teamAScoreSpy);
  });
});
