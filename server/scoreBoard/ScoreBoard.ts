import {EventEmitter} from 'events';
import GameEvents from '../GameEvents';
import ScoreBoardsEvents from "./ScoreBoardEvents";

export default class ScoreBoard {
  static POINTS_REQUIRED_TO_WIN: number = 5;

  private teamAPoints: number = 0;
  private teamBPoints: number = 0;

  constructor(
    private gameEvent: EventEmitter,
  ) {

    this.playerAScores = this.playerAScores.bind(this);
    this.playerBScores = this.playerBScores.bind(this);

    gameEvent.on(GameEvents.playerAScore, this.playerAScores);
    gameEvent.on(GameEvents.playerBScore, this.playerBScores);
  }

  private playerAScores() {
    this.teamAPoints++;
    this.gameEvent.emit(ScoreBoardsEvents.updateTeamAScore, this.teamAPoints);
    this.checkForEndGameCondition();

  }
  private playerBScores() {
    this.teamBPoints++;
    this.gameEvent.emit(ScoreBoardsEvents.updateTeamBScore, this.teamBPoints);
    this.checkForEndGameCondition();

  }
  private checkForEndGameCondition() {
    if (this.teamAPoints >= ScoreBoard.POINTS_REQUIRED_TO_WIN) {
      this.gameEvent.emit(GameEvents.teamAWin);

    } else if (this.teamBPoints >= ScoreBoard.POINTS_REQUIRED_TO_WIN) {
      this.gameEvent.emit(GameEvents.teamBWin);
    }

  }

}
