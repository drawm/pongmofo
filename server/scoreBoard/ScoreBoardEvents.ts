const ScoreBoardsEvents = {
  updateTeamAScore:'updateTeamAScore',
  updateTeamBScore:'updateTeamBScore',
};
export default ScoreBoardsEvents;