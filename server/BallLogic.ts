import {find} from 'lodash';
import {EventEmitter} from 'events';
import Vector from './utils/Vector';
import BallEvents from './BallEvents';
import Rectangle from './utils/Rectangle';
import GameEvents from "./GameEvents";

export default class BallLogic {
  private static MAX_SPEED: number = 25;

  constructor(
    private eventEmitter: EventEmitter,
    private ball: Rectangle,
    private playerA: Rectangle,
    private playerB: Rectangle,
    private stage: Rectangle,
    private velocity: Vector,
    private acceleration: number = 1.1,
  ) {
  }

  private accelerate(): void {
    if (Math.abs(this.velocity.x) + Math.abs(this.velocity.y) < BallLogic.MAX_SPEED << 1) {
      this.velocity = Vector.times(this.acceleration, this.velocity);
      this.velocity.x = Math.min(BallLogic.MAX_SPEED, this.velocity.x);
      this.velocity.y = Math.min(BallLogic.MAX_SPEED, this.velocity.y);
      this.eventEmitter.emit(BallEvents.velocity, this.velocity);
    }
  }

  private isMovingTowardTheCenter(velocity: Vector, position: Vector, stage) {
    return (velocity.x > 0 && position.x < (stage.width >> 1)) ||
      (velocity.x < 0 && position.x > (stage.width >> 1));
  }

  public move(): void {

    // Prevent the ball from bouncing rapidly off the wall and the paddle
    if (!this.isMovingTowardTheCenter(this.velocity, this.ball, this.stage)) {
      const obstacles: Rectangle[] = [
        this.playerA,
        this.playerB,
      ];

      const obstacle: Rectangle = find(obstacles, obstacle => this.isColliding(this.ball, this.velocity, obstacle));

      if (obstacle) {
        this.bounceX();
        this.accelerate();
      } else {

        // Bounce or hit a wall
        if (this.isTouchingLeftWall(this.ball, this.stage)) {
          this.eventEmitter.emit(GameEvents.playerBScore);
          this.bounceX();

        } else if (this.isTouchingRightWall(this.ball, this.stage)) {
          this.eventEmitter.emit(GameEvents.playerAScore);
          this.bounceX();

        }
      }
    }

    if (this.ball.y === 0 || this.ball.y === this.stage.height - this.ball.height) {
      this.bounceY()
    }

    // Update the ball's position
    this.ball = this.applyVelocityToBall(this.ball, this.velocity, this.stage);
    this.eventEmitter.emit(BallEvents.move, new Vector(this.ball.x, this.ball.y));

  }

  static getRandomVelocity() {
    let velocity;
    do {
      velocity = new Vector(1, Math.random() * 2 - 1)
    } while (Math.abs(velocity.x) < .1 || Math.abs(velocity.y) < .1);

    // Skew the velocity to make sure we do not go at a 90* angle
    return Vector.times(5.5, velocity);
  }

  private bounceX() {
    this.velocity.x *= -1;
    this.eventEmitter.emit(BallEvents.velocity, this.velocity);
  }

  private bounceY() {
    this.velocity.y *= -1;
    this.eventEmitter.emit(BallEvents.velocity, this.velocity);
  }

  private isTouchingLeftWall(ball: Rectangle, stage: Rectangle): boolean {
    return ball.x <= stage.x;
  }

  private isTouchingRightWall(ball: Rectangle, stage: Rectangle): boolean {
    return ball.x >= stage.x + (stage.width - ball.width);

  }

  private applyVelocityToBall(ball: Rectangle, velocity: Vector, stage): Rectangle {
    // Movement without obstacle
    const position = Vector.plus(ball as Vector, velocity);
    position.x = Math.max(0, Math.min(position.x, stage.width - ball.width));
    position.y = Math.max(0, Math.min(position.y, stage.height - ball.height));

    return new Rectangle(position.x, position.y, ball.width, ball.height);
  }

  private isColliding(ball: Rectangle, velocity: Vector, obstacle: Rectangle): boolean {
    return Rectangle.dirtyCollision(ball, obstacle) ||
      Rectangle.willTraverse(ball, velocity, obstacle);
  }
}