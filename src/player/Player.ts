import PlayerEvents from './PlayerEvents';
import {EventEmitter} from 'events';
import Vector from "../utils/Vector";

export default class Player extends PIXI.Graphics {

  constructor(color: number, width: number, height: number) {
    super();
    this.beginFill(color);
    this.drawRect(0, 0, width, height);
    this.endFill();

    this.move = this.move.bind(this);
  }

  private move(position:Vector){
    this.x = position.x;
    this.y = position.y;
  }

  setController(controller: EventEmitter) {
    controller.on(PlayerEvents.move, this.move);
  }
}