export default class Vector {
  static zero: Vector = new Vector(0, 0, 0);
  static left: Vector = new Vector(-1, 0, 0);
  static right: Vector = new Vector(1, 0, 0);
  static up: Vector = new Vector(0, -1, 0);
  static down: Vector = new Vector(0, 1, 0);

  constructor(public x: number, public y: number, public z: number = 0) {
  }

  static times(multiplier: number, vector: Vector) {
    return new Vector(multiplier * vector.x, multiplier * vector.y, multiplier * vector.z);
  }

  static minus(vectorA: Vector, vectorB: Vector) {
    return new Vector(vectorA.x - vectorB.x, vectorA.y - vectorB.y, vectorA.z - vectorB.z);
  }

  static plus(vectorA: Vector, vectorB: Vector) {
    return new Vector(vectorA.x + vectorB.x, vectorA.y + vectorB.y, vectorA.z + vectorB.z);
  }

  static dot(vectorA: Vector, vectorB: Vector) {
    return vectorA.x * vectorB.x + vectorA.y * vectorB.y + vectorA.z * vectorB.z;
  }

  static mag(vector: Vector) {
    return Math.sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
  }

  static norm(vector: Vector) {
    const mag = Vector.mag(vector);
    const div = (mag === 0) ? Infinity : 1.0 / mag;
    return Vector.times(div, vector);
  }

  static cross(vectorA: Vector, vectorB: Vector) {
    return new Vector(
      vectorA.y * vectorB.z - vectorA.z * vectorB.y,
      vectorA.z * vectorB.x - vectorA.x * vectorB.z,
      vectorA.x * vectorB.y - vectorA.y * vectorB.x
    );
  }

  toString(){
    return `{ x:${this.x}, y:${this.y}, z:${this.z} }`;
  }

}