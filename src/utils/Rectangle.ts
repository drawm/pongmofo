export default class Rectangle {
  constructor(public x: number = 0, public y: number = 0, public width: number = 0, public height: number = 0) {
  }

  public static dirtyCollision(A: Rectangle, B: Rectangle): boolean {
    const aCenterX: number = A.x + (  A.width >> 1);
    const bCenterX: number = B.x + (  B.width >> 1);

    const aCenterY: number = A.y + ( A.height >> 1);
    const bCenterY: number = B.y + ( B.height >> 1);

    return (Math.abs(aCenterX - bCenterX) <= Math.max(A.width, B.width) >> 1) &&
      ( Math.abs(aCenterY - bCenterY) <= Math.max(A.height, B.height) >> 1);

  }
}