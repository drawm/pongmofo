import {EventEmitter} from 'events';
import {Text, Container} from 'pixi.js';
import Player from "./player/Player";
import Ball from "./ball/Ball";
import Rectangle from "./utils/Rectangle";

export default class Pong {
  private playerA: Player;
  private playerB: Player;
  private stage: Container;
  private ball: Ball;
  private playerAScoreboard: Text;
  private playerBScoreboard: Text;
  private playerBScore: number;
  private playerAScore: number;

  constructor(private renderer) {
    this.render = this.render.bind(this);

    this.onPlayerBScore = this.onPlayerBScore.bind(this);
    this.onPlayerAScore = this.onPlayerAScore.bind(this);
    this.onPlayerAWin = this.onPlayerAWin.bind(this);
    this.onPlayerBWin = this.onPlayerBWin.bind(this);

    this.stage = new Container();
  }

  start(
    ballController: EventEmitter,
    ballRectangle: Rectangle,
  ) {
    this.playerAScore = 0;
    this.playerBScore = 0;

    this.initTexts();
    this.initScoreboards();

    this.ball = new Ball(0x2a2a2a, ballRectangle.width + ballRectangle.height >> 2);
    this.ball.setController(ballController);
    this.ball.x = ballRectangle.x;
    this.ball.y = ballRectangle.y;
    this.stage.addChild(this.ball);

  }

  private initScoreboards() {
    this.playerAScoreboard = new Text(this.playerAScore.toString(), {
      fontSize : '132px',
      fill : '#cbcbcb',
    });
    this.stage.addChild(this.playerAScoreboard);

    this.playerBScoreboard = new Text(this.playerBScore.toString(), {
      fontSize : '132px',
      fill : '#cbcbcb',
    });
    this.stage.addChild(this.playerBScoreboard);

    this.updateScoreBoardsPosition();
  }

  private initTexts() {
    const style = {
      fontFamily : 'Arial',
      fontSize : '36px',
      fontStyle : 'italic',
      fontWeight : 'bold',
      fill : '#F7EDCA',
      stroke : '#4a1850',
      strokeThickness : 5,
      dropShadow : true,
      dropShadowColor : '#000000',
      dropShadowAngle : Math.PI / 6,
      dropShadowDistance : 6,
      wordWrap : true,
      wordWrapWidth : 440
    };

    const basicText = new Text('PONG');
    const richText = new Text('mofo!', style);

    // Place both text at the center with just enough offset to balance both width
    basicText.x = (this.renderer.width - basicText.width - richText.width) >> 1;
    basicText.y = (this.renderer.height - basicText.height) >> 1;

    richText.x = basicText.x + basicText.width;
    richText.y = basicText.y - (richText.height - basicText.height >> 1);

    this.stage.addChildAt(basicText, 0);
    this.stage.addChildAt(richText, 0);
  }

  public render() {
    this.renderer.render(this.stage);
  }

  private onPlayerAWin() {
    this.renderer.backgroundColor = 0x91de91;
  }

  private onPlayerBWin() {
    this.renderer.backgroundColor = 0xde9191;
  }

  private onPlayerAScore() {
    this.playerAScore++;
    this.playerAScoreboard.text = this.playerAScore.toString();
    this.updateScoreBoardsPosition();
  }

  private onPlayerBScore() {
    this.playerBScore++;
    this.playerBScoreboard.text = this.playerBScore.toString();
    this.updateScoreBoardsPosition();
  }

  private updateScoreBoardsPosition() {
    this.playerAScoreboard.x = (this.renderer.width - this.playerAScoreboard.width) >> 2;
    this.playerAScoreboard.y = (this.renderer.height - this.playerAScoreboard.height) >> 1;
    this.playerBScoreboard.x = this.renderer.width - this.playerBScoreboard.width - ((this.renderer.width - this.playerBScoreboard.width) >> 2);
    this.playerBScoreboard.y = (this.renderer.height - this.playerBScoreboard.height) >> 1;
  }
}
