import {EventEmitter} from 'events';
import Vector from '../utils/Vector';
import BallEvents from '../ball/BallEvents';

export default class Ball extends PIXI.Graphics {
  private velocity: Vector = new Vector(0,0);

  constructor(color: number, radius: number) {
    super();
    this.beginFill(color);
    this.drawCircle(radius, radius, radius);
    this.endFill();

    this.move = this.move.bind(this);
  }

  private setVelocity(velocities:Vector[]){
    this.velocity = velocities[0];
  }

  private move(position:Vector[]){
    this.x = position[0].x;
    this.y = position[0].y;
  }

  setController(controller: EventEmitter) {
    controller.on(BallEvents.move, this.move);
    controller.on(BallEvents.velocity, this.setVelocity);
  }
}