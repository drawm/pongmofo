import {EventEmitter} from 'events';
import {autoDetectRenderer, Point, Container, Graphics} from 'pixi.js';
import Pong from "./Pong";
import Rectangle from "./utils/Rectangle";
import NetworkEmitter from "./NetworkEmitter";

const WIDTH = 800;
const HEIGHT = 500;

const renderer = autoDetectRenderer(WIDTH, HEIGHT, {backgroundColor : 0xefefef});
document.body.appendChild(renderer.view);

const test = new Container();
const a = new Graphics();
a.beginFill(0xff0000);
a.drawRect(0, 0, 1, 1);
a.endFill();
test.addChild(a);


const b = new Graphics();
b.beginFill(0xff0000);
b.drawRect(4, 4, 1, 1);
b.endFill();
test.addChild(b);

renderer.render(test);

// Initial position
const ballRectangle = new Rectangle((WIDTH - 20) >> 1, (HEIGHT - 20) >> 1, 20, 20);

// Server connection
// Server socket
const eventEmitter = new NetworkEmitter({
    hostname : '0.0.0.0', // Server's IP
    port : 4000, // Server's port
    encoder : 'json', // Server's encoder
});

const pong = new Pong(renderer);

pong.start(eventEmitter, ballRectangle);


const tick = () => {
    requestAnimationFrame(tick);
    pong.render();
};

tick();
